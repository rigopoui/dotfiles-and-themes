## This repository contains my config files and scripts.
![screnshot](screen.png)

You will find my i3 config in the **desktop** directory and a similar sway config in the **laptop** directory.
The i3 config may be a bit older than sway, it's not used as much these days. I aim to fully migrate to sway in the future.

You can also find a few simple _quality of life_ scripts that I make use of, in the **desktop | laptop/.local/bin/** directory.

The **old-stuff** directory contains lxqt/openbox and sddm themes
