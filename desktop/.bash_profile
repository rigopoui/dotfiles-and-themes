#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
export HISTSIZE=5000
export HISTSIZESIZE=5000
export HISTIGNORE="exa:ls:uptime:clear:nofap:pwd"
export EDITOR=nvim
