#!/bin/bash
WALLDIR=~/Pictures/wallpapers/
WALL=$(ls $WALLDIR | fzf -e)
if [ ! -z "$WALL" ];
then
    feh --bg-fill "$WALLDIR$WALL"
    cp "$WALLDIR$WALL" $XDG_CONFIG_HOME/i3/wallpaper.png
fi
